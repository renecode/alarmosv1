/*
  AlarmV1.h - Library for Standard Alarm Implementation on Lab
  Created by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/

#ifndef AlarmV1_h
#define AlarmV1_h

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#include "dhtV1.h"
#include "mq135V1.h"

//extern dhtV1 tempSensor;
//extern mq135V1 O2Sensor;

//safe Alarm Mode assignment
#define MODOARMADO 1 //B00000000 //Modo ARMADO
#define MODODESARMADO 2 //B00000001 //Modo DESARMADO
#define MODOTEST 3 //B00000010 //Modo TEST
//Print some outputs (HIGH/LOW)
#define FLAGDEBTIMER LOW
#define FLAGSTATUS LOW
  class AlarmV1
  {
  public:
		//Constructor
  	AlarmV1(){};
		//Destructor
  	~AlarmV1(){};

		//Main func
  	void begin(void);
  	void update(void);

		//Set initial conditions
  	void set_buttons( byte buttonArma, byte buttonDesa, byte buttonTest,byte ledArma, byte ledDesa, byte ledTest);
  	void set_alarm(byte pinAlarm,byte sensor1Pin,byte sensor2Pin);
  	
		//Action Functions
  	byte get_button(void);
  	void sonarAlarma(byte pinSonar, byte sonarEnabled);
  	int timerRestoreActivado(int timeFinalVal, int timeProgress);
  	byte checkPreset(void);
		//Modos de funcionamiento alarma
  	void armadoFun(void);
  	void desarmadoFun(void);
  	void testFun(void);

		//Public variables
  	byte buttonActualState[3],buttonReading[3],buttonPrevious[3];
  	int alarmMode;
  	long tmpDebounceTime[3], debounceTime; 
  	byte sonarFlag, sonarPermanenteFlag, alarmTimer;
  	int finalValCountDown[2],tmpModeTimer[2];
  	dhtV1 dthTempVal;
  	mq135V1 o2SetVal;
  private:
		//Private variables
  	byte  _pinButtons[3],_pinLeds[3];
  	byte _pinAlarm;
  	byte _sensorPin[2];
  	float _thresSensor[2];
  };

#endif



