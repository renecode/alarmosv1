/*
  dhtV1.h - Library for Standard Alarm Implementation on Lab
  Adapted from http://playground.arduino.cc/Main/DHTLib
  by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/

#ifndef dhtV1_h
#define dhtV1_h

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif

#define DHTLIB_OK                0
#define DHTLIB_ERROR_CHECKSUM   -1
#define DHTLIB_ERROR_TIMEOUT    -2
#define DHTLIB_INVALID_VALUE    -999

#define DHTLIB_DHT11_WAKEUP     18
#define DHTLIB_DHT22_WAKEUP     1

#define FLAGSTATUSDTH LOW 
  class dhtV1
  {
  public:
    int readDth(uint8_t pin);
    void setPreset(float userPreset);
    float getPreset();
    byte checkIt(void);
    float measureIt(uint8_t pin);
    float humidity;
    float temperature;

  private:
   float _preseTemp, _presetHum;
    uint8_t bits[5];  // buffer to receive data
    int read(uint8_t pin, uint8_t wakeupDelay);
  };
#endif
