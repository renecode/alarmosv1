/*
  dhtV1.cpp - Library for Standard Alarm Implementation on Lab
  Adapted from http://playground.arduino.cc/Main/DHTLib
  by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/

#include "dhtV1.h"

// max timeout is 100usec.
// For a 16Mhz ()proc that is max 1600 clock cycles
// loops using TIMEOUT use at least 4 clock cycli
// so 100 us takes max 400 loops
// so by dividing F_CPU by 40000 we "fail" as fast as possible
#define TIMEOUT (F_CPU/40000)


/////////////////////////////////////////////////////
//
// PUBLIC
//
  int dhtV1::readDth(uint8_t pin)
  {
    // READ VALUES
    int rv = read(pin, DHTLIB_DHT22_WAKEUP);
    if (rv != DHTLIB_OK)
    {
        humidity    = DHTLIB_INVALID_VALUE;  // invalid value, or is NaN prefered?
        temperature = DHTLIB_INVALID_VALUE;  // invalid value
        return rv; // propagate error value
    }
    // CONVERT AND STORE
    humidity = word(bits[0], bits[1]) * 0.1;
    //A word stores a 16-bit unsigned number, from 0 to 65535. Same as an unsigned int. 
    //word(h, l) 
    //h: the high-order (leftmost) byte of the word
    //l: the low-order (rightmost) byte of the word 
    if (bits[2] & 0x80) // negative temperature
    {
        temperature = -0.1 * word(bits[2] & 0x7F, bits[3]);
    }
    else
    {
        temperature = 0.1 * word(bits[2], bits[3]);
    }

    // TEST CHECKSUM
    uint8_t sum = bits[0] + bits[1] + bits[2] + bits[3];
    if (bits[4] != sum) return DHTLIB_ERROR_CHECKSUM;

    return DHTLIB_OK;
}

void dhtV1::setPreset(float userPreset)
{
    _preseTemp = userPreset;
}

float dhtV1::getPreset()
{
    return _preseTemp;
}

float dhtV1::measureIt(uint8_t pin){
    readDth(pin);
    if(FLAGSTATUSDTH) Serial.println("-----------------"); 
    if(FLAGSTATUSDTH) Serial.print("Temp: ");  
    if(FLAGSTATUSDTH) Serial.println(temperature);  
    return temperature;
}

byte dhtV1::checkIt(void)
{   
    if (temperature < _preseTemp)
    {
        if(FLAGSTATUSDTH) Serial.println("No se activo x temp");  
        if(FLAGSTATUSDTH) Serial.println("-----------------"); 
        return LOW;
    }
    if (temperature >= _preseTemp)
    {
        if(FLAGSTATUSDTH) Serial.println("Se activo x temp");  
        if(FLAGSTATUSDTH) Serial.println("-----------------"); 
        return HIGH;
    }
}
/////////////////////////////////////////////////////
//
// PRIVATE
//

// return values:
// DHTLIB_OK
// DHTLIB_ERROR_TIMEOUT
int dhtV1::read(uint8_t pin, uint8_t wakeupDelay)
{
    // INIT BUFFERVAR TO RECEIVE DATA
    uint8_t mask = 128;
    uint8_t idx = 0;

    // EMPTY BUFFER
    for (uint8_t i = 0; i < 5; i++) bits[i] = 0;

    // REQUEST SAMPLE
        pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
    delay(wakeupDelay);
    digitalWrite(pin, HIGH);
    delayMicroseconds(40);
    pinMode(pin, INPUT);

    // GET ACKNOWLEDGE or TIMEOUT
    unsigned int loopCnt = TIMEOUT;
    while(digitalRead(pin) == LOW)
    {
        if (--loopCnt == 0) return DHTLIB_ERROR_TIMEOUT;
    }

    loopCnt = TIMEOUT;
    while(digitalRead(pin) == HIGH)
    {
        if (--loopCnt == 0) return DHTLIB_ERROR_TIMEOUT;
    }

    // READ THE OUTPUT - 40 BITS => 5 BYTES
    for (uint8_t i = 0; i < 40; i++)
    {
        loopCnt = TIMEOUT;
        while(digitalRead(pin) == LOW)
        {
            if (--loopCnt == 0) return DHTLIB_ERROR_TIMEOUT;
        }

        unsigned long t = micros();

        loopCnt = TIMEOUT;
        while(digitalRead(pin) == HIGH)
        {
            if (--loopCnt == 0) return DHTLIB_ERROR_TIMEOUT;
        }

        if ((micros() - t) > 40) bits[idx] |= mask;
        mask >>= 1;
        if (mask == 0)   // next byte?
        {
            mask = 128;
            idx++;
        }
    }
    return DHTLIB_OK;
}
