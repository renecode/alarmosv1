/*
  mq135V1.cpp - Library for Standard Alarm Implementation on Lab
  Created by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/
#include "mq135V1.h"

  void mq135V1::setPreset(byte userPreset)
  {
    _presetPpm = userPreset;
}

float mq135V1::getPreset()
{
    return _presetPpm;
}

byte mq135V1::measureIt(uint8_t pin)
{

    ppm = digitalRead(pin);
    if(FLAGSTATUSMQ) Serial.println("-----------------");  
    if(FLAGSTATUSMQ) Serial.println("O2");  
    return (ppm);
} 

byte mq135V1::checkIt(void)
{   
    if (ppm)
    {
        if(FLAGSTATUSMQ) Serial.println("No se activo x O2"); 
        if(FLAGSTATUSMQ) Serial.println("-----------------");  
        return LOW;
    }
    else 
    {
        if(FLAGSTATUSMQ) Serial.println("Se activo x O2");
        if(FLAGSTATUSMQ) Serial.println("-----------------");   
        return HIGH;
    }
}