/*
  mq135V1.h - Library for Standard Alarm Implementation on Lab
  Created by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/
#ifndef mq135V1_h
#define mq135V1_h

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif
#define FLAGSTATUSMQ LOW
  class mq135V1
  {
  public:
  	void setPreset(byte userPreset);
  	float getPreset();
  	byte checkIt(void);
  	byte measureIt(uint8_t pin);
  	byte ppm;
  private:   
  	byte _presetPpm;

  };
#endif
//
// END OF FILE
//