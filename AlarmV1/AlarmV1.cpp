/*
  AlarmV1.cpp - Library for Standard Alarm Implementation on Lab
  Created by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
*/

#include "AlarmV1.h"

  void AlarmV1::set_buttons(byte buttonArma, byte buttonDesa, byte buttonTest,byte ledArma, byte ledDesa, byte ledTest)
  {

    _pinButtons[0] = buttonArma;
    _pinButtons[1] = buttonDesa;
    _pinButtons[2] = buttonTest;
    _pinLeds[0] = ledArma;
    _pinLeds[1] = ledDesa;
    _pinLeds[2] = ledTest;
    for(int itera=0; itera<3;itera++){
      pinMode(_pinButtons[itera], INPUT);
      delay(200);
      pinMode(_pinLeds[itera], OUTPUT);
      delay(300);
    }

    buttonReading[0] = LOW;
    buttonReading[1] = LOW;
    buttonReading[2] = LOW;

    buttonPrevious[0] = HIGH;
    buttonPrevious[1] = HIGH;
    buttonPrevious[2] = HIGH;

    tmpDebounceTime[0] = 0;
    tmpDebounceTime[1] = 0;
    tmpDebounceTime[2] = 0;
    debounceTime = 200;

  }

  void AlarmV1::set_alarm(byte pinAlarm,byte sensor1Pin,byte sensor2Pin)
  {
   _pinAlarm = pinAlarm;
   pinMode(pinAlarm, OUTPUT);

	alarmTimer = LOW; //para la cuenta regresiva
  sonarPermanenteFlag = LOW;
  sonarFlag = LOW; 
  
	_sensorPin[0] = sensor1Pin; //Temp
	_sensorPin[1] = sensor2Pin; //O2
	_thresSensor[0] = 0.0; //Temp
	_thresSensor[1] = 0.0; //O2

	finalValCountDown[0] = 1000; //Test cycles till armado
	finalValCountDown[1] = 2000; //Desarmado cycles till armado
	tmpModeTimer[0] = 0;
	tmpModeTimer[1] = 0;
  dthTempVal.setPreset(40.0);
  o2SetVal.setPreset(LOW);
  alarmMode = MODOARMADO;
}
void AlarmV1::begin(void){
	delay(1000);
  set_buttons(5,6,7,10,11,12); //set_buttons(Button arma, desarma, test, led arma, led desarma, led test);
	set_alarm(13,2,3); //set_alarm(Alarm Pin, sensor1 Pin,sensor2 Pin);
  delay(500);
  armadoFun();
  delay(100);
}

void AlarmV1::update(void)
{
  delay(100); 
  byte tmp = get_button();
  delay(100);
  if(FLAGSTATUS)  Serial.println(tmp);
  switch (tmp) {
    case MODOARMADO:
    armadoFun();
    break;
    case MODODESARMADO:
    desarmadoFun();
    break;
    case MODOTEST:
    testFun();
    break;
  }

	//delay(500);

  sonarAlarma(_pinAlarm, sonarFlag);
}

byte AlarmV1::get_button(void)
{
	boolean onceFlag_ = false;

	for(int itera=0; itera<3;itera++)
	{
   buttonReading[itera] = digitalRead(_pinButtons[itera]);
/*
// Si el pin de entrada cambia de LOW-> HIGH
// y paso suficiente tiempo desde el último cambio
// (para ignorar ruido) cambiamos se cambia el Modo
// de la alarma (alarmMode) y se almacena el tiempo (tmpDebounceTime[i])
// Notes: se podria usar una unica variable tmpDebounceTime para los tres modos
*/ 
   if(! onceFlag_ ){
     if (buttonReading[itera] == HIGH && buttonPrevious[itera] == LOW && millis() - tmpDebounceTime[itera] > debounceTime)
     {
				//safe Alarm Mode assignment
				//state[count] = LOW;

      switch(itera){
       case 0:
       alarmMode = MODOARMADO;
       break;
       case 1:
       alarmMode = MODODESARMADO;
       break;
       case 2:
       alarmMode = MODOTEST;
       break;
     }

     onceFlag_ = true;
     tmpDebounceTime[itera] = millis();    
   }
   buttonPrevious[itera] = buttonReading[itera];
 }
}
return alarmMode;
}

//Implementado con 1 sensor!
void AlarmV1::armadoFun()
{
  if(FLAGSTATUS) Serial.println("Modo Armado");
  
	//Solo se activa la alarma si recibe un trigger del sensor
	//lectura digital std.. otra variable con dig/analog
  sonarFlag = checkPreset();
  //sonarFlag = false;
  if(FLAGSTATUS) Serial.println("Estado sensor:");
  if(sonarFlag == 0){ 
   if(FLAGSTATUS) Serial.println("Nothing happens");
 }
 if(sonarFlag != 0){
  if(FLAGSTATUS) Serial.println("On fire");
}
if(sonarFlag == HIGH)
{
  sonarPermanenteFlag = HIGH;
}

if(sonarPermanenteFlag == HIGH)
{
 sonarFlag= HIGH;
 if(FLAGSTATUS) Serial.println("PITILLOO");
}

digitalWrite(_pinLeds[0], HIGH);
digitalWrite(_pinLeds[1], LOW);
digitalWrite(_pinLeds[2], LOW);

    //Reset clock Test
tmpModeTimer[0] = finalValCountDown[0];
    //Reset clock Desarmado
tmpModeTimer[1] = finalValCountDown[1];

delay(10);
}

void AlarmV1::desarmadoFun()
{
	//clock Desarmado: tmpModeTimer[1]
  if(FLAGSTATUS) Serial.println("Modo DESARMADO");
  sonarFlag = LOW;
  sonarPermanenteFlag = LOW;

  if(FLAGDEBTIMER)
  {
   Serial.println("DESARMADO: Antes Timer");
   Serial.println(tmpModeTimer[0]);
   Serial.println(tmpModeTimer[1]);
 }

 tmpModeTimer[1] = timerRestoreActivado(finalValCountDown[1],tmpModeTimer[1]);

 if(FLAGDEBTIMER)
 {
   Serial.println("DESARMADO: Despues Timer");
   Serial.println(tmpModeTimer[0]);
   Serial.println(tmpModeTimer[1]);
 }

    //Finaliza el Timer
 if(tmpModeTimer[1] == 0) 
 {
  alarmMode = MODOARMADO;
}

   	//Estado indicado x Led
if (alarmMode == MODODESARMADO)
{
 digitalWrite(_pinLeds[1], HIGH);
 digitalWrite(_pinLeds[0], LOW);
}
if (alarmMode == MODOARMADO)
{
 digitalWrite(_pinLeds[0], HIGH);
 digitalWrite(_pinLeds[1], LOW);
}
digitalWrite(_pinLeds[2], LOW);

    //Reset clock Test
tmpModeTimer[0] = finalValCountDown[0];

delay(10);
}
void AlarmV1::testFun()
{
	//clock Test: tmpModeTimer[0]
  if(FLAGSTATUS) Serial.println("Modo Test");

  if(FLAGDEBTIMER)
  {
   Serial.println("TEST: Antes Timer");
   Serial.println(tmpModeTimer[0]);
   Serial.println(tmpModeTimer[1]);
 }

 tmpModeTimer[0] = timerRestoreActivado(finalValCountDown[0],tmpModeTimer[0]);

 if(FLAGDEBTIMER)
 {
   Serial.println("TEST: Despues Timer");
   Serial.println(tmpModeTimer[0]);
   Serial.println(tmpModeTimer[1]);
 }

    //Set flag sonar -> Activar alarma!!
 if(tmpModeTimer[0] > 0){
  sonarFlag = HIGH;
}

    //Finaliza el Timer
if(tmpModeTimer[0] == 0) {
  sonarFlag = HIGH;
  alarmMode = MODOARMADO;
}

   	//Estado indicado x Led
if (alarmMode == MODOTEST)
{
 digitalWrite(_pinLeds[2], HIGH);
 digitalWrite(_pinLeds[0], LOW);
}
if (alarmMode == MODOARMADO)
{
 digitalWrite(_pinLeds[0], HIGH);
 digitalWrite(_pinLeds[2], LOW);
}
digitalWrite(_pinLeds[1], LOW);

    //Reset clock Desarmado
tmpModeTimer[1] = finalValCountDown[1];

delay(10);
}

void AlarmV1::sonarAlarma(byte pinSonar, byte sonarEnabled)
{
	if(sonarEnabled == HIGH)
	{
    //Implementacion de la alarma  
		for(int i=0;i<5;i++)
		{
			//prender el led con pinSonar
			digitalWrite(pinSonar, HIGH);
			delay(100);
			digitalWrite(pinSonar, LOW);
			delay(100);
		}
	}
}

int AlarmV1::timerRestoreActivado(int timeFinalVal, int timeProgress)
{
	int tempCountDown_ = 0;
  if((timeProgress <= timeFinalVal) && (timeProgress > 0)){
   tempCountDown_ = timeProgress - 1;
 }
 return tempCountDown_;
}

//Implementar lectura
byte AlarmV1::checkPreset()
{
  byte sonarflagTmp1_ = LOW;
  byte sonarflagTmp2_ = LOW;
  dthTempVal.measureIt(_sensorPin[0]);
  sonarflagTmp1_ = dthTempVal.checkIt();

  o2SetVal.measureIt(_sensorPin[1]);
  sonarflagTmp2_ = o2SetVal.checkIt();

  return (sonarflagTmp1_ || sonarflagTmp2_);
}

