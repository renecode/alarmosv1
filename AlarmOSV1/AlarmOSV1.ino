/*
  AlarmOSV1.ino - Main Arduino program for Standard Alarm Implementation on Lab
  Created by René Cejas Bolecek, September 16, 2014.
  reneczechdev@gmail.com
  Electronic & Software design in colaboration with Ignacio Artola 
  Supported by the Low Temperatures Laboratory, Comisión Nacional de Energía Atómica, Bariloche,ARGENTINA
  Version 1.0
*/

#include "AlarmV1.h"

  AlarmV1 alarm;

  void setup() {
    Serial.begin(9600);
    delay(200);
    alarm.begin();
    
  }

  void loop() {
    alarm.update();
  }

